package fx;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import server.ServerHandler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MyApp extends Application {
    private static final int INET_PORT = 52443;
    private static final int BOSS_N_THREADS = 1;
    private static final int WORKER_N_THREADS = 4;
    private static final int BUFFER_SIZE = 8196;

    Label response;

    static EventLoopGroup bossGroup;
    static EventLoopGroup workerGroup;
    static ServerBootstrap b;
    static ChannelFuture f;

    public static void main(String[] args) {


        launch("go go");


    }

    @Override
    public void start(Stage primaryStage) throws InterruptedException {

        primaryStage.setTitle("FX L");

        FlowPane rootNode = new FlowPane(10, 10);
        rootNode.setAlignment(Pos.CENTER);

        Scene scene = new Scene(rootNode, 1000, 500);

        primaryStage.setScene(scene);


        response = new Label("push a button");

        Button b1 = new Button("start server");
        Button b2 = new Button("stop server");

        b1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                bossGroup = null;
                workerGroup = null;
                b = null;
                f = null;


                try {

                    bossGroup = new NioEventLoopGroup(BOSS_N_THREADS);
                    workerGroup = new NioEventLoopGroup(WORKER_N_THREADS);
                    b = new ServerBootstrap();

                    b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class).childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            socketChannel.pipeline().addLast(new ServerHandler());
                        }
                    }).option(ChannelOption.SO_BACKLOG, 128).childOption(ChannelOption.RCVBUF_ALLOCATOR, new FixedRecvByteBufAllocator(BUFFER_SIZE));

                    // Bind and start to accept incoming connections.
                    f = b.bind(INET_PORT).sync();

                    // Wait until the server socket is closed.
                    // In this example, this does not happen, but you can do that to gracefully
                    // shut down your server.

                    response.setText("server is ON");

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });

//        b2.setOnAction(event -> response.setText("world was pressed"));
        b2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (f != null && workerGroup != null && bossGroup != null) {
                    f.channel().close();

                    workerGroup.shutdownGracefully();
                    bossGroup.shutdownGracefully();

                    response.setText("server is OFF");
                } else {
                    response.setText("server is stopped, alo...");

                }
            }
        });


        rootNode.getChildren().addAll(b1, b2, response);

        primaryStage.show();


    }

}
