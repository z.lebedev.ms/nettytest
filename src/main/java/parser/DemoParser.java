package parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class DemoParser {
    public static void main(String[] args) {
        byte[] bytes = getBytesFromFile("src\\main\\resources\\filesSendByClient\\Data2");

        System.out.println(Arrays.toString(bytes));
        Parser.parsGlobalPacket(bytes);

    }

    private static byte[] getBytesFromFile(String path) {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String s;
            while ((s = br.readLine()) != null) {
                sb.append(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] sArray = sb.toString().split(" ");

        byte[] bytes = new byte[sArray.length];
        for (int i = 0; i < sArray.length; i++) {
            bytes[i] = Integer.valueOf(sArray[i], 16).byteValue();
        }
        return bytes;
    }
}
