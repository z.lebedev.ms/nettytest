package parser;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

public class MiniLogFile {
    public static void serialize(String path, Set<byte[]> set) {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path)))) {
            for (byte[] bytes : set) {
                bw.write(Arrays.toString(bytes).concat(System.lineSeparator()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
