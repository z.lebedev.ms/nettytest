package parser;

public class DemoDate {
    public static void main(String[] args) {
        byte[] bytes = {(byte) 0xec, (byte) 0xd3, (byte) 0x8a, 0x0};
        byte b0 = bytes[0];
        byte b1 = bytes[1];
        byte b2 = bytes[2];
        byte b3 = bytes[3];


        int y = ((b3 & 0xff) << 16) | ((b2 & 0xff) << 8) | (b1 & 0xff);
        System.out.println(y);
        int secs = y * 60 + (b0 & 0xff);
        System.out.println("secs = " + secs);

    }
}
