package option1;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.util.ReferenceCountUtil;


public class ClientHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ByteBuf firstMessage = Unpooled.buffer();

        byte[] bytes = {(byte) 0x96, 0x52, 0x54, 0x56, 0x62, 0x5, 0x0, 0x0, 0x0, (byte) 0xd1, 0x2f, 0x1, 0x0, 0x1, 0x18, 0x33, 0x63, 0x4a, (byte) 0xf0, (byte) 0x8e, 0x10, 0x3, 0x0, (byte) 0xc2, (byte) 0x98, 0x50, 0x28, 0x0, 0x0, 0x0, 0x0, 0x0};

        firstMessage.writeBytes(bytes);

        ctx.write(firstMessage); //  alternative:  ctx.writeAndFlush(msg);
        ctx.flush();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        ByteBuf in = (ByteBuf) msg;
        try {
            System.out.println("response:");
            while (in.isReadable()) {                          //      alternative for loop:
                System.out.println(in.readByte());             //      System.out.println(in.toString(CharsetUtil.UTF_8));
                System.out.flush();
            }
        } finally {
            ReferenceCountUtil.release(msg); // (2)
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}