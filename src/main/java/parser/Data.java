package parser;

import java.util.HashSet;
import java.util.Set;

public class Data extends Thread {
    private static final int INTERVAL = 1800000;      //interval for writing data from set to file, ms
    private static Set<byte[]> set = new HashSet<>();

    public static synchronized Set<byte[]> getSet() {
        return set;
    }

    @Override
    public void run() {
        while (true) {
            MiniLogFile.serialize("src\\main\\resources\\set", set);
            try {
                sleep(INTERVAL);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
