package parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class Parser {
    private static final int PREFIX_LENGTH = 32;
    private static final int HOURLY_PACKET_LENGTH = 64;
    private static final int EMERGENCY_PACKET_LENGTH = 32;
    private static final int DAILY_PACKET_LENGTH = 64;
    private static final int INTERVENTION_PACKET_LENGTH = 32;
    private static final int PROGRAMMABLE_PARAMETERS_PACKET_LENGTH = 528;
    private static final int NEW_BLOCK_TELEMETRY_IMOD_PACKET_LENGTH = 64;
    private static final int HOURLY_UNCLOSED_DAY_PACKET_LENGTH = 64;
    private static final int EMERGENCY_UNCLOSED_DAY_LENGTH = 24;
    private static final int INTERVENTION_UNCLOSED_DAY_PACKET_LENGTH = 24;
    private static final int READING_PACKET_LENGTH = 479;                           //  length? CRC?
    private static final int EMPTY_PACKET_LENGTH = 3;                               //  CRC?
    private static final int GIVE_BLOCK_NEW_VERSION_IMOD_PACKET_LENGTH = 9;         //  ?
    private static final int EMERGENCY_TUNING_NEW_VERSION_IMOD_PACKET_LENGTH = 8;   //  ?
    private static final int NO_DATA_PACKET_LENGTH = 6;                             //  ?
    private static final int BLOCK_TELEMETRY_IMOD_OLD_FORMAT_PACKET_LENGTH = 32;
    private static final int DATA_MPAN = 531;

    private static int globalPacketSize;

    private static int currentProducer = 0x1;
    private static int currentDevice = 0x17;

    final static Logger logger = LoggerFactory.getLogger(Parser.class);

    public static void parsGlobalPacket(byte[] bytes) {
//        Data.getSet().add(bytes);
        logger.warn("got {} bytes", String.valueOf(bytes.length));

        if (bytes.length < PREFIX_LENGTH) {
            logger.error("too small size of the global packet ");        // writing to log
        } else {
            //checking packet size;
            byte b4 = bytes[4];
            byte b5 = bytes[5];

            globalPacketSize = ((b5 & 0xff) << 8) | (b4 & 0xff);

            if (bytes.length != globalPacketSize) {
                logger.error("wrong size of the global packet ");     // writing to log
                return;
            }
            logger.info("global packet size = {}", globalPacketSize);

            //checking main CRC
            if (!checkingCRC(bytes)) {
                logger.error("main CRC is wrong ");            // writing to log

            } else {
                logger.info("main CRC is ok");
                logger.info("packet: {}", Arrays.toString(bytes));

                // checking with current device
                byte b13 = bytes[13];       // producer
                byte b14 = bytes[14];       // type of device
                if (currentProducer != b13 | currentDevice != b14) {

                    logger.info("request device in bases");

                }

                // parsing included packets
                int index = PREFIX_LENGTH;
                while (index < globalPacketSize - 2) {
                    switch (bytes[index]) {
                        case 0x1:
                            byte[] daily = new byte[DAILY_PACKET_LENGTH];
                            System.arraycopy(bytes, index, daily, 0, daily.length);
                            parseDaily(daily);
                            index += daily.length;
                            break;
                        case 0x2:
                            byte[] hourly = new byte[HOURLY_PACKET_LENGTH];
                            System.arraycopy(bytes, index, hourly, 0, hourly.length);
                            parseHourly(hourly);
                            index += hourly.length;
                            break;
                        case 0x3:
                            byte[] emergency = new byte[EMERGENCY_PACKET_LENGTH];
                            System.arraycopy(bytes, index, emergency, 0, emergency.length);
                            parseEmergency(emergency);
                            index += emergency.length;
                            break;
                        case 0x4:
                            byte[] intervention = new byte[INTERVENTION_PACKET_LENGTH];
                            System.arraycopy(bytes, index, intervention, 0, intervention.length);
                            parseIntervention(intervention);
                            index += intervention.length;
                            break;
                        case 0x5:
                            byte[] programmableParameters = new byte[PROGRAMMABLE_PARAMETERS_PACKET_LENGTH];
                            System.arraycopy(bytes, index, programmableParameters, 0, programmableParameters.length);
                            parseProgrammableParameters(programmableParameters);
                            index += programmableParameters.length;
                            break;
                        case 0x6:
                            byte[] hourlyUnclosedDay = new byte[HOURLY_UNCLOSED_DAY_PACKET_LENGTH];
                            System.arraycopy(bytes, index, hourlyUnclosedDay, 0, hourlyUnclosedDay.length);
                            parseHourlyUnclosedDay(hourlyUnclosedDay);
                            index += hourlyUnclosedDay.length;
                            break;
                        case 0x7:
                            byte[] emergencyUnclosedDay = new byte[EMERGENCY_UNCLOSED_DAY_LENGTH];
                            System.arraycopy(bytes, index, emergencyUnclosedDay, 0, emergencyUnclosedDay.length);
                            parseEmergencyUnclosedDay(emergencyUnclosedDay);
                            index += emergencyUnclosedDay.length;
                            break;
                        case 0x8:
                            byte[] interventionUnclosedDay = new byte[INTERVENTION_UNCLOSED_DAY_PACKET_LENGTH];
                            System.arraycopy(bytes, index, interventionUnclosedDay, 0, interventionUnclosedDay.length);
                            parseInterventionUnclosedDay(interventionUnclosedDay);
                            index += interventionUnclosedDay.length;
                            break;
                        case 0x9:
                            byte[] reading = new byte[READING_PACKET_LENGTH];
                            System.arraycopy(bytes, index, reading, 0, reading.length);
                            parseReading(reading);
                            index += reading.length;
                            break;
                        case 0xa:
                            byte[] empty = new byte[EMPTY_PACKET_LENGTH];
                            System.arraycopy(bytes, index, empty, 0, empty.length);
                            parseEmpty(empty);
                            index += EMPTY_PACKET_LENGTH;
                            break;
                        case 0xb:
//                            Внимание!!! При выдаче блоков “выдать блок новой версии ПО iMod” и “авария
//                            новой версии ПО iMod” в префиксе сообщения код операции - 2
                            byte[] giveBlockNewVersionIMod = new byte[GIVE_BLOCK_NEW_VERSION_IMOD_PACKET_LENGTH];
                            System.arraycopy(bytes, index, giveBlockNewVersionIMod, 0, giveBlockNewVersionIMod.length);
                            parseGiveBlockNewVersionIMod(giveBlockNewVersionIMod);
                            index += giveBlockNewVersionIMod.length;
                            break;
                        case 0xc:
                            byte[] emergencyTuningNewVersionIMod = new byte[EMERGENCY_TUNING_NEW_VERSION_IMOD_PACKET_LENGTH];
                            System.arraycopy(bytes, index, emergencyTuningNewVersionIMod, 0, emergencyTuningNewVersionIMod.length);
                            parseEmergencyTuningNewVersionIMod(emergencyTuningNewVersionIMod);
                            index += emergencyTuningNewVersionIMod.length;
                            break;
                        case 0xd:
//                            Нет данных (запрошенных суток нет в базе - только для режима докачки)
                            byte[] noData = new byte[NO_DATA_PACKET_LENGTH];
                            System.arraycopy(bytes, index, noData, 0, noData.length);
                            parseNoData(noData);
                            index += noData.length;
                            break;
                        case 0xf:
                            byte[] newBlockTelemetryIMod = new byte[NEW_BLOCK_TELEMETRY_IMOD_PACKET_LENGTH];
                            System.arraycopy(bytes, index, newBlockTelemetryIMod, 0, newBlockTelemetryIMod.length);
                            parseNewBlockTelemetryIMod(newBlockTelemetryIMod);
                            index += newBlockTelemetryIMod.length;
                            break;
                        case 0xe:
                            byte[] blockTelemetryIModOldFormat = new byte[BLOCK_TELEMETRY_IMOD_OLD_FORMAT_PACKET_LENGTH];
                            System.arraycopy(bytes, index, blockTelemetryIModOldFormat, 0, blockTelemetryIModOldFormat.length);
                            parseBlockTelemetryIModOldFormat(blockTelemetryIModOldFormat);
                            index += blockTelemetryIModOldFormat.length;
                            break;
                        case 0x65:
                            byte[] dataMPAN = new byte[DATA_MPAN];
                            System.arraycopy(bytes, index, dataMPAN, 0, dataMPAN.length);
                            parseDataMPAN(dataMPAN);
                            index += dataMPAN.length;
                            break;
                    }
                }
            }
        }
    }

    private static void parseDataMPAN(byte[] dataMPAN) {
        if (checkingCRC(dataMPAN)) {       //checking CRC
            logger.info("data MPAN packet CRC is ok");
        } else {
            logger.error("data MPAN packet CRC is wrong");      // writing to log
        }
    }

    private static void parseBlockTelemetryIModOldFormat(byte[] blockTelemetryIModOldFormat) {
        if (checkingCRC(blockTelemetryIModOldFormat)) {       //checking CRC
            logger.info("block telemetry iMod old format packet CRC is ok");
        } else {
            logger.error("block telemetry iMod old format packet CRC is wrong");      // writing to log
        }
    }

    private static void parseNoData(byte[] noData) {
        if (checkingCRC(noData)) {                       //checking CRC
            logger.info("no data packet CRC is ok");
        } else {
            logger.error("no data packet CRC is wrong");      // writing to log
        }
    }

    private static void parseEmergencyTuningNewVersionIMod(byte[] emergencyTuningNewVersionIMod) {
        if (checkingCRC(emergencyTuningNewVersionIMod)) {       //checking CRC
            logger.info("emergency tuning new version iMod packet CRC is ok");
        } else {
            logger.error("emergency tuning new version iMod packet CRC is wrong");      // writing to log
        }
    }

    private static void parseGiveBlockNewVersionIMod(byte[] giveBlockNewVersionIMod) {
        if (checkingCRC(giveBlockNewVersionIMod)) {       //checking CRC
            logger.info("give block new version iMod packet CRC is ok");
        } else {
            logger.error("give block new version iMod packet CRC is wrong");      // writing to log
        }
    }

    private static void parseEmpty(byte[] empty) {
        if (checkingCRC(empty)) {       //checking CRC
            logger.info("empty packet CRC is ok");
        } else {
            logger.error("empty packet CRC is wrong");      // writing to log
        }
    }

    private static void parseReading(byte[] reading) {
        if (checkingCRC(reading)) {       //checking CRC
            logger.info("reading packet CRC is ok");
        } else {
            logger.error("reading packet CRC is wrong");      // writing to log
        }
    }

    private static void parseInterventionUnclosedDay(byte[] interventionUnclosedDay) {
        logger.warn("intervention unclosed day - NO Description");

        // NO Description
    }

    private static void parseEmergencyUnclosedDay(byte[] emergencyUnclosedDay) {
        logger.warn("emergency unclosed day - NO Description");

        // NO Description
    }

    private static void parseHourlyUnclosedDay(byte[] hourlyUnclosedDay) {
        logger.warn("hourly unclosed day - NO Description");

        // NO Description
    }

    private static void parseDaily(byte[] daily) {
        if (checkingCRC(daily)) {       //checking CRC
            logger.info("daily packet CRC is ok");
        } else {
            logger.error("daily packet CRC is wrong");      // writing to log
        }
    }

    private static void parseEmergency(byte[] emergency) {
        if (checkingCRC(emergency)) {       //checking CRC
            logger.info("emergency packet CRC is ok");
        } else {
            logger.error("emergency packet CRC is wrong");      // writing to log
        }
    }

    private static void parseHourly(byte[] hourly) {
        if (checkingCRC(hourly)) {       //checking CRC
            logger.info("hourly packet CRC is ok");
        } else {
            logger.error("hourly packet CRC is wrong");      // writing to log
        }
    }

    private static void parseIntervention(byte[] intervention) {
        if (checkingCRC(intervention)) {       //checking CRC
            logger.info("intervention packet CRC is ok");
        } else {
            logger.error("intervention packet CRC is wrong");      // writing to log
        }
    }

    private static void parseProgrammableParameters(byte[] programmableParameters) {
        if (checkingCRC(programmableParameters)) {       //checking CRC
            logger.info("programmable parameters packet CRC is ok");
        } else {
            logger.error("programmable parameters packet CRC is wrong");      // writing to log
        }
    }

    private static void parseNewBlockTelemetryIMod(byte[] newBlockTelemetryIMod) {
        if (checkingCRC(newBlockTelemetryIMod)) {       //checking CRC
            logger.info("new block telemetry iMod packet CRC is ok");
        } else {
            logger.error("new block telemetry iMod packet CRC is wrong");      // writing to log
        }
    }

    private static boolean checkingCRC(byte[] array) {
        byte beforeLast = array[array.length - 2];
        byte last = array[array.length - 1];
        int controlSumActual = ((last & 0xff) << 8) | (beforeLast & 0xff);

        byte[] forCalculateControlSum = new byte[array.length - 2];
        System.arraycopy(array, 0, forCalculateControlSum, 0, array.length - 2);
        int controlSumCalculated = Crc.calculateCRC(forCalculateControlSum);

        return controlSumActual == controlSumCalculated;
    }


//    enum BlockType {
//        EMPTY(1),
//        DAILY(64),
//        HOURLY(64),
//        EMERGENCY(32),
//        INTERVENTION(32),
//        PP_REPORTS(528),
//        ONE_HOUR(64),
//        ONE_EMERGENCY(24),
//        ONE_INTERVENTION(24);
//
//        int index;
//        int size;
//
//        BlockType(int size) {
//            this.index = ordinal();
//            this.size = size;
//        }
//    }

}
