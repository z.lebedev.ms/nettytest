package fx;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import parser.Parser;

public class ServerHandler extends ChannelInboundHandlerAdapter {

    private static final String PATH_RECEIVED_FILES = "src\\main\\resources\\filesReceivedByServer\\";
    private static final byte[] RESPONSE = {104, 101, 108, 108, 111}; //hello

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        ByteBuf in = (ByteBuf) msg;
        byte[] receivedMessage = new byte[in.writerIndex()];
        try {
            for (int i = 0; i < receivedMessage.length; i++) {
                receivedMessage[i] = in.readByte();
            }
            Parser.parsGlobalPacket(receivedMessage);
        } finally {
            ReferenceCountUtil.release(msg);
        }


//        //write to file
//        String s = ctx.channel().id().toString();
//        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(PATH_RECEIVED_FILES.concat(s), true))) {
//            ByteBuf in = (ByteBuf) msg;
//            try {
//                while (in.isReadable()) {
//                    bos.write(in.readByte());
//                }
//            } finally {
//                ReferenceCountUtil.release(msg);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        //test response
//        byte[] response = new byte[32];
//        byte[] resp1 = {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48};
//        byte[] resp2 = {49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49};
//        byte[] resp3 = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96};
//        if (receivedMessage.length > 31 && (receivedMessage[9] == (-47)) & (receivedMessage[10] == 47) ) {
//            response = resp1;
//        } else if (receivedMessage.length > 31 && (receivedMessage[9] == (80)) & (receivedMessage[10] == -61) ) {
//            response = resp2;
//        } else {
//            response = resp3;
//        }

//        response
        ByteBuf firstMessage = Unpooled.buffer();
        firstMessage.writeBytes(RESPONSE);
        ChannelFuture f = ctx.write(firstMessage);
        ctx.flush();

        f.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) {
                assert f == future;
                ctx.close();
            }
        });
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }
}
