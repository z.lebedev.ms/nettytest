package option1;


import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Handles a server-side channel.
 */
public class ServerHandler extends ChannelInboundHandlerAdapter {
//    private SensorWorker worker;
//
//    @Override
//    public void channelActive(ChannelHandlerContext ctx) throws Exception {
//        worker = new SensorWorker(ctx.channel());
//    }


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {

        String s = ctx.channel().id().toString();
        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("src\\main\\resources\\nettyFiles\\".concat(s), true))) {

            ByteBuf in = (ByteBuf) msg;

//            System.out.println(in);
            try {
                while (in.isReadable()) {
//                    System.out.println(in.readerIndex());

                    bos.write(in.readByte());
//                    System.out.print(" ");
//                    System.out.flush();

                }
                //      alternative for loop:
//                System.out.println(in.toString(CharsetUtil.UTF_8));
            } finally {
                ReferenceCountUtil.release(msg); // (2)
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        //response
        ByteBuf responseMessage = Unpooled.buffer();

        //32 bytes:
        byte[] bytes = {(byte) 0x96, 0x52, 0x54, 0x56, 0x62, 0x5, 0x0, 0x0, 0x0, (byte) 0xd1, 0x2f, 0x1, 0x0, 0x1, 0x18, 0x33, 0x63, 0x4a, (byte) 0xf0, (byte) 0x8e, 0x10, 0x3, 0x0, (byte) 0xc2, (byte) 0x98, 0x50, 0x28, 0x0, 0x0, 0x0, 0x0, 0x0};

        responseMessage.writeBytes(bytes);

        ChannelFuture f = ctx.write(responseMessage); //  alternative:  ctx.writeAndFlush(msg);
        ctx.flush();

        f.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) {
                assert f == future;
                ctx.close();
            }
        });

    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }
}